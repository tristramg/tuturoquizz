# Tuturoquizz : apprenez à placer les sous-préfectures en vous marrant

## Développement

Les projet est codé avec vue.js et maplibre.

Lancer en dev :

```
npm install
npm run dev
```

Pour un build de production

```
npm run build
```

## Les données

Les sous-préfectures sont extraites de wikidata avec la requête SPARQL suivante :


```
SELECT ?commune ?communeLabel ?pop ?departementLabel ?cheflieuLabel ?code ?coord
WHERE
{
  ?pref wdt:P31 wd:Q179831. # Doit être une préfecture
  ?pref wdt:P527 ?souspref.
  ?pref wdt:P1001 ?departement.
  ?departement wdt:P36 ?cheflieu.
  ?departement wdt:P2586 ?code.
  ?souspref wdt:P1001 ?arrondissement.
  ?arrondissement wdt:P36 ?commune.
  ?commune wdt:P625 ?coord.
  OPTIONAL { ?commune wdt:P1082 ?pop.}
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } # le label viendra de préférence dans votre langue, et autrement en anglais
}
```

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).
